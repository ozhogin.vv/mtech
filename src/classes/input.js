class Input {
    constructor(type, name, style_class, placeholder, options) {
        this.type = type;
        this.name = name;
        this.style_class = style_class;
        this.placeholder = placeholder;
        this.options = options;
    }
}

module.exports = {Input};
