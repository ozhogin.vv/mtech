class Button {
    constructor(type, button_class, title, event) {
        this.type = type;
        this.button_class = button_class;
        this.title = title;
        this.event = event;
    }
}

module.exports = {Button};
